const mongoose = require('mongoose');
let User = mongoose.model('User', {
    name: {
        type: String,
        required: true,
        trim: true,
        minlength: 1,
    },
    email: {
        required: true,
        trim: true,
        type: String,
        minlength: 1,
        unique: true,
    },
    empID:{
        required: true,
        trim: true,
        type: String,
        minlength: 2,
        unique: true,
    },
    password: {
        required: true,
        type: String,
        minlength: 1
    },
    userType: {
        type: String,
        required: true,
    }
});

module.exports = {
    User
};