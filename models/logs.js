var mongoose = require('mongoose');
let Logs = mongoose.model('Logs', {
    user:{
        type: String,
        required:true
    },
    date: {
        type: String,
        required: true
    },
    hours: {
        type: Number
    },
    project: {
        type: String,
    },
    activities: {
        type: String,
        trim: true
    }
});
module.exports = {
    Logs
};