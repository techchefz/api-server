const mongoose = require('mongoose');
let Projects = mongoose.model('Projects', {
    name: {
        type: String,
        required: true,
        trim: true,
        minlength: 1,
        unique:true
    },
});

module.exports = {
    Projects
};