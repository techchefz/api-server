const express = require('express');
const bodyParser = require('body-parser');
const {
    mongoose
} = require('./db/mongoose');
const {
    Logs
} = require('./models/logs');
const {
    User
} = require('./models/user');
const {
    ObjectID
} = require('mongodb');
const {
    Projects
} = require('./models/projects');

const app = express();

app.use(bodyParser.json());


// New User SignUp 
app.post('/signup', (req, res) => {
    const user = new User({
        name: req.body.name,
        email: req.body.email,
        empID: req.body.empID,
        password: req.body.password,
        userType: req.body.userType,
    });
    user.save()
        .then((doc) => {
            res.sendStatus(200);
        }, (e) => {
            res.status(400).send();
        })
        .catch(() => {
            res.sendStatus(400);
        });
});

// User SignIn  
app.post('/login', (req, res) => {
    let email = req.body.email;
    let password = req.body.password;

    User.findOne({ email: email })
        .then((user) => {

            if (user.length == 0 || user == undefined || user == null) {
                res.sendStatus(400);
            } else if (user.password == password) {
                res.sendStatus(200);
            } else {
                res.sendStatus(401);
            }
        }, (e) => {
            res.status(400).send();
        })
        .catch((error) => {
            res.sendStatus(400);
        });
});

// Api to save user-work in the database
app.post('/update-logs', (req, res) => {
    Logs.insertMany(req.body.saved).then((doc) => {
        console.log(doc);
        res.sendStatus(200);
    }, (e) => {
        res.sendStatus(400);
    })
        .catch(() => {
            res.sendStatus(400);
        });
});

// Add Projects
app.post('/add-project', (req, res) => {
    const project = new Projects({
        name: req.body.project,
    });
    project.save().then((doc) => {
        res.sendStatus(200);
    }, (e) => {
        res.sendStatus(400);
    })
        .catch(() => {
            res.sendStatus(400);
        });
});

// Get Projects 
app.get('/get-projects', (req, res) => {
    Projects.find()
        .then((project) => {
            let arr = [];
            project.forEach((pro) => {
                arr.push(pro.name);
            });
            res.send(arr);
        })
        .catch((error) => {
            res.send(error)
        });
});

// Get User Details
app.get('/get-user-details', (req, res) => {
    User.find()
        .then((userDetails) => {
            res.send(userDetails);
        })
        .catch((err) => {
            res.sendStatus(400);
        });
});

// Get Work done by user and other details
app.post('/get-user-log', (req, res) => {
    let user = req.body.user;
    Logs.find({ user: user })
        .then((data) => {
            console.log('====================================');
            console.log(data);
            console.log('====================================');
            const returnobj = {
                success: false,
                message: "",
                data: {}
            }
            if (data.length != 0) {
                returnobj.success = true,
                    returnobj.message = "Found and sent",
                    returnobj.data = data
                res.send(returnobj);
            } else {
                res.send(returnobj);
            }
        }).catch((err) => {
            res.sendStatus(400);
        })
});

//Page Not Found
app.get('*', (req, res) => {
    res.send('404 Page not found');
});

app.listen(3000, () => {
    console.log('Server started on port 3000');
});